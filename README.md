# SDK Java JAX-RS - Frete V1 para API do Lojista

Durante a integração com o Marketplace da Cnova, há momentos onde existe a necessidade de comunicação ou consulta de informações específicas situadas na plataforma do lojista.

![Fluxo da API do Lojista](http://cnovaportaldev.sensedia.com/api-portal/sites/default/files/images/sdk_serv.png)

A estrutura de código aqui apresentada tem o objetivo de fornecer uma implementação básica para construção do recurso de consulta de fretes diretamente no lojista, podendo ser utilizada apenas como referência de implementação.

## Desenvolvimento

Os fontes desse projeto apenas expõe uma interface Restful no servidor onde o WAR for implantado.

Para integração com seu sistema, procure pela classe _com/cnova/seller/api/freight/v1/impl/FreightApiServiceImpl.java_ e faça as implementações necessárias.

Nela você poderá fazer a consulta do CEP informado por parâmetro e retornar a estrutura solcitada na documentação do recurso.

### Exemplo de contrução de retorno

O bloco abaixo representa uma estrutura de valores de frete consultados para um CEP específico, de acordo com as classes de modelo presentes nessa implementação.

```java
public Response getFreight(String skuId,String zipCode)
throws NotFoundException {

	GetFreightResponse r = new GetFreightResponse();

	Freight f1 = new Freight();
	f1.setSkuIdOrigin("92837");
	f1.setQuantity(1);
	f1.setFreightAmount(Double.valueOf(5D));
	f1.setDeliveryTime(3);
	f1.setFreightType(FreightTypeEnum.NORMAL);

	r.getFreights().add(f1);

	Freight f2 = new Freight();
	f2.setSkuIdOrigin("92837");
	f2.setQuantity(1);
	f2.setFreightAmount(Double.valueOf(19D));
	f2.setDeliveryTime(3);
	f2.setFreightType(FreightTypeEnum.NORMAL);

	r.getFreights().add(f2);

	r.setSellerMpToken("8sjoAJjs93d7S63Sa98US72");
	r.setFreightAdditionalInfo("Transportadora XPTO");

	return Response.ok().entity(r).build();
}
```