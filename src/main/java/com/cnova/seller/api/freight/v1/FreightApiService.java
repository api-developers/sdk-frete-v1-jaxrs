package com.cnova.seller.api.freight.v1;

import com.cnova.seller.api.freight.v1.*;
import com.cnova.seller.api.freight.v1.model.*;

import com.sun.jersey.multipart.FormDataParam;

import com.cnova.seller.api.freight.v1.model.GetFreightResponse;

import java.util.List;
import com.cnova.seller.api.freight.v1.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;

public abstract class FreightApiService {
  
    public abstract Response getFreight(String skuId,String zipCode)
    throws NotFoundException;
  
}
