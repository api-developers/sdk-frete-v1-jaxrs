package com.cnova.seller.api.freight.v1.factories;

import com.cnova.seller.api.freight.v1.FreightApiService;
import com.cnova.seller.api.freight.v1.impl.FreightApiServiceImpl;

public class FreightApiServiceFactory {

    private final static FreightApiService service = new FreightApiServiceImpl();

    public static FreightApiService getFreightApi()
    {
        return service;
    }
}
