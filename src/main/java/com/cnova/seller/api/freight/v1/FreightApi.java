package com.cnova.seller.api.freight.v1;

import com.cnova.seller.api.freight.v1.model.*;
import com.cnova.seller.api.freight.v1.FreightApiService;
import com.cnova.seller.api.freight.v1.factories.FreightApiServiceFactory;

import io.swagger.annotations.ApiParam;

import com.sun.jersey.multipart.FormDataParam;

import com.cnova.seller.api.freight.v1.model.GetFreightResponse;

import java.util.List;
import com.cnova.seller.api.freight.v1.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;
import javax.ws.rs.*;

@Path("/freight")
@Consumes({ "application/json" })
@Produces({ "application/json" })
@io.swagger.annotations.Api(value = "/freight", description = "the freight API")
public class FreightApi  {

    private final FreightApiService delegate = FreightApiServiceFactory.getFreightApi();
    
    @GET
    
    
    
    @io.swagger.annotations.ApiOperation(value = "", notes = "Recupera as informações de frete para um ou mais produto em um determinado CEP.", response = GetFreightResponse.class)
    @io.swagger.annotations.ApiResponses(value = { 
    @io.swagger.annotations.ApiResponse(code = 200, message = "Resposta da consulta de frete") })
    public Response getFreight(@ApiParam(value = "Combinação de SKU ID do produto do Lojista e quantidade separados por virgula. Várias combinações podem ser passadas separadas por |.",required=true) @QueryParam("skuId") String skuId,
    @ApiParam(value = "CEP de entrega",required=true) @QueryParam("zipCode") String zipCode)
    throws NotFoundException {
        return delegate.getFreight(skuId,zipCode);
    }
}


