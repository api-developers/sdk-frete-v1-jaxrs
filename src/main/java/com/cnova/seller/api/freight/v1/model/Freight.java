package com.cnova.seller.api.freight.v1.model;

import com.cnova.seller.api.freight.v1.model.ScheduledFreightDateOptions;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class Freight  {
  
  private String skuIdOrigin = null;
  private Integer quantity = null;
  private Double freightAmount = null;
  private Integer deliveryTime = null;
  public enum FreightTypeEnum {
     NORMAL,  EXPRESSA,  AGENDADA, 
  };
  private FreightTypeEnum freightType = null;
  private ScheduledFreightDateOptions scheduledFreightDateOptions = null;

  
  /**
   * SKU ID do produto do lojista
   **/
  @ApiModelProperty(required = true, value = "SKU ID do produto do lojista")
  @JsonProperty("skuIdOrigin")
  public String getSkuIdOrigin() {
    return skuIdOrigin;
  }
  public void setSkuIdOrigin(String skuIdOrigin) {
    this.skuIdOrigin = skuIdOrigin;
  }

  
  /**
   * Quantidade de produtos utilizada para o cálculo
   **/
  @ApiModelProperty(required = true, value = "Quantidade de produtos utilizada para o cálculo")
  @JsonProperty("quantity")
  public Integer getQuantity() {
    return quantity;
  }
  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  
  /**
   * Valor do frete
   **/
  @ApiModelProperty(required = true, value = "Valor do frete")
  @JsonProperty("freightAmount")
  public Double getFreightAmount() {
    return freightAmount;
  }
  public void setFreightAmount(Double freightAmount) {
    this.freightAmount = freightAmount;
  }

  
  /**
   * Prazo de entrega em dias úteis
   **/
  @ApiModelProperty(required = true, value = "Prazo de entrega em dias úteis")
  @JsonProperty("deliveryTime")
  public Integer getDeliveryTime() {
    return deliveryTime;
  }
  public void setDeliveryTime(Integer deliveryTime) {
    this.deliveryTime = deliveryTime;
  }

  
  /**
   * Tipo de frete (NORMAL,EXPRESSA, AGENDADA)
   **/
  @ApiModelProperty(required = true, value = "Tipo de frete (NORMAL,EXPRESSA, AGENDADA)")
  @JsonProperty("freightType")
  public FreightTypeEnum getFreightType() {
    return freightType;
  }
  public void setFreightType(FreightTypeEnum freightType) {
    this.freightType = freightType;
  }

  
  /**
   * Lista de opções para a data agendada
   **/
  @ApiModelProperty(value = "Lista de opções para a data agendada")
  @JsonProperty("scheduledFreightDateOptions")
  public ScheduledFreightDateOptions getScheduledFreightDateOptions() {
    return scheduledFreightDateOptions;
  }
  public void setScheduledFreightDateOptions(ScheduledFreightDateOptions scheduledFreightDateOptions) {
    this.scheduledFreightDateOptions = scheduledFreightDateOptions;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Freight {\n");
    
    sb.append("  skuIdOrigin: ").append(skuIdOrigin).append("\n");
    sb.append("  quantity: ").append(quantity).append("\n");
    sb.append("  freightAmount: ").append(freightAmount).append("\n");
    sb.append("  deliveryTime: ").append(deliveryTime).append("\n");
    sb.append("  freightType: ").append(freightType).append("\n");
    sb.append("  scheduledFreightDateOptions: ").append(scheduledFreightDateOptions).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
