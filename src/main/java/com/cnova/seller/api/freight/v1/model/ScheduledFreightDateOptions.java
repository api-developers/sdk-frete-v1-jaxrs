package com.cnova.seller.api.freight.v1.model;

import java.util.Date;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class ScheduledFreightDateOptions  {
  
  private Date startDate = null;
  private Date endDate = null;
  public enum AvailablePeriodsEnum {
     MANHA,  TARDE,  NOITE, 
  };
  private AvailablePeriodsEnum availablePeriods = null;

  
  /**
   * Data de inicio da opção
   **/
  @ApiModelProperty(required = true, value = "Data de inicio da opção")
  @JsonProperty("startDate")
  public Date getStartDate() {
    return startDate;
  }
  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  
  /**
   * Data de termino da opção
   **/
  @ApiModelProperty(required = true, value = "Data de termino da opção")
  @JsonProperty("endDate")
  public Date getEndDate() {
    return endDate;
  }
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  
  /**
   * Lista de períodos disponíveis (MANHA,TARDE,NOITE) separados por vírgula
   **/
  @ApiModelProperty(required = true, value = "Lista de períodos disponíveis (MANHA,TARDE,NOITE) separados por vírgula")
  @JsonProperty("availablePeriods")
  public AvailablePeriodsEnum getAvailablePeriods() {
    return availablePeriods;
  }
  public void setAvailablePeriods(AvailablePeriodsEnum availablePeriods) {
    this.availablePeriods = availablePeriods;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduledFreightDateOptions {\n");
    
    sb.append("  startDate: ").append(startDate).append("\n");
    sb.append("  endDate: ").append(endDate).append("\n");
    sb.append("  availablePeriods: ").append(availablePeriods).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
