package com.cnova.seller.api.freight.v1.model;

import com.cnova.seller.api.freight.v1.model.Freight;
import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class GetFreightResponse  {
  
  private List<Freight> freights = new ArrayList<Freight>() ;
  private String freightAdditionalInfo = null;
  private String sellerMpToken = null;

  
  /**
   * Data e hora que o evento ocorreu
   **/
  @ApiModelProperty(required = true, value = "Data e hora que o evento ocorreu")
  @JsonProperty("freights")
  public List<Freight> getFreights() {
    return freights;
  }
  public void setFreights(List<Freight> freights) {
    this.freights = freights;
  }

  
  /**
   * nformações adicionais da consulta de frete, como o código da transportadora
   **/
  @ApiModelProperty(required = true, value = "nformações adicionais da consulta de frete, como o código da transportadora")
  @JsonProperty("freightAdditionalInfo")
  public String getFreightAdditionalInfo() {
    return freightAdditionalInfo;
  }
  public void setFreightAdditionalInfo(String freightAdditionalInfo) {
    this.freightAdditionalInfo = freightAdditionalInfo;
  }

  
  /**
   * Token de identificação do lojista no parceiro. Para garantir origem da informação. Deve ser utilizado o mesmo Auth-Token das chamadas à API.
   **/
  @ApiModelProperty(required = true, value = "Token de identificação do lojista no parceiro. Para garantir origem da informação. Deve ser utilizado o mesmo Auth-Token das chamadas à API.")
  @JsonProperty("sellerMpToken")
  public String getSellerMpToken() {
    return sellerMpToken;
  }
  public void setSellerMpToken(String sellerMpToken) {
    this.sellerMpToken = sellerMpToken;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFreightResponse {\n");
    
    sb.append("  freights: ").append(freights).append("\n");
    sb.append("  freightAdditionalInfo: ").append(freightAdditionalInfo).append("\n");
    sb.append("  sellerMpToken: ").append(sellerMpToken).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
